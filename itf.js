"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LOG = exports.mkdir = void 0;
var fs_1 = require("fs");
var path_1 = require("path");
var method_1 = require("@mac-xiang/method");
var sf = [",", "\n", "\r"];
function mkdir(path) {
    var t = path.replace(/\\/g, "/");
    var cwd = process.cwd().replace(/\\/g, "/");
    var fullPath = t.indexOf(cwd) == 0 ? t : path_1.resolve(cwd, t).replace(/\\/g, "/");
    var pa = fullPath.split("/");
    var dir;
    var l = pa.length + 1;
    for (var i = 1; i < l; i++) {
        dir = pa.slice(0, i).join("/");
        if (fs_1.existsSync(dir)) {
            t = fs_1.statSync(dir);
            if (!t.isDirectory())
                fs_1.unlinkSync(dir);
        }
        if (!fs_1.existsSync(dir))
            fs_1.mkdirSync(dir);
    }
}
exports.mkdir = mkdir;
var LOG = /** @class */ (function () {
    function LOG(filePath, suffix) {
        if (suffix === void 0) { suffix = ".log"; }
        this.dir = this.getPath(filePath);
        this.suffix = suffix;
    }
    LOG.prototype.getPath = function (filePath) {
        var r = path_1.resolve(__dirname, filePath ? filePath : path_1.resolve(process.cwd(), "logs/chats"));
        if (fs_1.existsSync(r)) {
            if (fs_1.statSync(r).isFile()) {
                fs_1.unlinkSync(r);
                fs_1.mkdirSync(r);
            }
        }
        else {
            mkdir(r);
        }
        return r;
    };
    LOG.prototype.getFileName = function (p) {
        var ret = p;
        if (ret.lastIndexOf(this.suffix) < 0 || ret.lastIndexOf(this.suffix) != ret.length - this.suffix.length) {
            ret += this.suffix;
        }
        return path_1.resolve(this.dir, ret);
    };
    LOG.prototype.readLog = function (file) {
        var f = this.getFileName(file);
        if (fs_1.existsSync(f) && fs_1.statSync(f).isFile()) {
            var buffer = fs_1.readFileSync(f, "utf8"); // readFileSync的第一个参数是文件名
            while (sf.indexOf(buffer.substr(-1)) >= 0) {
                buffer = buffer.substr(0, buffer.length - 1);
            }
            return JSON.parse("[" + buffer + "]");
        }
    };
    LOG.prototype.write = function (file, data, line, insert) {
        if (line === void 0) { line = -1; }
        if (insert === void 0) { insert = true; }
        if (typeof data != "undefined") {
            var f = this.getFileName(file), text = JSON.stringify(data);
            if (line < 0 && fs_1.existsSync(f)) {
                fs_1.appendFileSync(f, text + ",\n");
            }
            else {
                var buffer = [];
                if (fs_1.existsSync(f)) {
                    buffer = this.readLog(f).map(function (a) {
                        return JSON.stringify(a);
                    });
                    buffer.splice(line, insert ? 0 : 1, text);
                }
                else {
                    buffer = Array.isArray(data) ? data.map(function (a) { return JSON.stringify(a); }) : [text];
                }
                fs_1.writeFileSync(f, buffer.join(",\n") + ",\n");
            }
        }
    };
    Object.defineProperty(LOG.prototype, "newLog", {
        get: function () {
            var ret = "";
            do {
                ret = method_1.uuid;
            } while (fs_1.existsSync(this.getFileName(ret)));
            return ret;
        },
        enumerable: false,
        configurable: true
    });
    return LOG;
}());
exports.LOG = LOG;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRmLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRmLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUdBLHlCQUE4RztBQUM5Ryw2QkFBK0I7QUFDL0IsNENBQXlDO0FBQ3pDLElBQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztBQUM3QixTQUFnQixLQUFLLENBQUMsSUFBWTtJQUNoQyxJQUFJLENBQUMsR0FBUSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN0QyxJQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM5QyxJQUFNLFFBQVEsR0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDdkYsSUFBTSxFQUFFLEdBQWEsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QyxJQUFJLEdBQVcsQ0FBQztJQUNoQixJQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1FBQzFCLEdBQUcsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsSUFBSSxlQUFVLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDbkIsQ0FBQyxHQUFHLGFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRTtnQkFBRSxlQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsZUFBVSxDQUFDLEdBQUcsQ0FBQztZQUFFLGNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUN0QztBQUNILENBQUM7QUFmRCxzQkFlQztBQUNEO0lBSUUsYUFBYSxRQUFpQixFQUFFLE1BQWU7UUFBZix1QkFBQSxFQUFBLGVBQWU7UUFDN0MsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFDRCxxQkFBTyxHQUFQLFVBQVEsUUFBaUI7UUFDdkIsSUFBTSxDQUFDLEdBQUcsY0FBTyxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsY0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO1FBQ3pGLElBQUksZUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2pCLElBQUksYUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUN4QixlQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2QsY0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2Q7U0FDRjthQUFNO1lBQ0wsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7UUFDRCxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7SUFDRCx5QkFBVyxHQUFYLFVBQVksQ0FBUztRQUNuQixJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDWixJQUFJLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO1lBQ3ZHLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDO1NBQ3BCO1FBQ0QsT0FBTyxjQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBQ0QscUJBQU8sR0FBUCxVQUFRLElBQVk7UUFDbEIsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxJQUFJLGVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxhQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDekMsSUFBSSxNQUFNLEdBQUcsaUJBQVksQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyx5QkFBeUI7WUFDL0QsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDekMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDOUM7WUFDRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBSSxNQUFNLE1BQUcsQ0FBUyxDQUFDO1NBQzFDO0lBQ0gsQ0FBQztJQUNELG1CQUFLLEdBQUwsVUFBTSxJQUFZLEVBQUUsSUFBUyxFQUFFLElBQVMsRUFBRSxNQUFhO1FBQXhCLHFCQUFBLEVBQUEsUUFBUSxDQUFDO1FBQUUsdUJBQUEsRUFBQSxhQUFhO1FBQ3JELElBQUksT0FBTyxJQUFJLElBQUksV0FBVyxFQUFFO1lBQzlCLElBQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQzlCLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlCLElBQUksSUFBSSxHQUFHLENBQUMsSUFBSSxlQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzdCLG1CQUFjLENBQUMsQ0FBQyxFQUFFLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQzthQUNqQztpQkFBTTtnQkFDTCxJQUFJLE1BQU0sR0FBYSxFQUFFLENBQUM7Z0JBQzFCLElBQUksZUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNqQixNQUFNLEdBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDO3dCQUNwQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzNCLENBQUMsQ0FBQyxDQUFDO29CQUNILE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQzNDO3FCQUFNO29CQUNMLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFNLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN0RjtnQkFDRCxrQkFBYSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDO2FBQzlDO1NBQ0Y7SUFDSCxDQUFDO0lBQ0Qsc0JBQUksdUJBQU07YUFBVjtZQUNFLElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQztZQUNiLEdBQUc7Z0JBQ0QsR0FBRyxHQUFHLGFBQUksQ0FBQzthQUNaLFFBQVEsZUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUM1QyxPQUFPLEdBQUcsQ0FBQztRQUNiLENBQUM7OztPQUFBO0lBQ0gsVUFBQztBQUFELENBQUMsQUFoRUQsSUFnRUM7QUFoRVksa0JBQUcifQ==