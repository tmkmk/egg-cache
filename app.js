"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TmkCache = void 0;
var tslib_1 = require("tslib");
var method_1 = require("@mac-xiang/method");
function getNode(node) {
    var r = [];
    var t = [];
    if (Array.isArray(node)) {
        t = node;
    }
    else if (typeof node == "string") {
        node.split(".").forEach(function (a) {
            if (a.length) {
                t.push(a);
            }
        });
    }
    else if (typeof node == "number") {
        t.push(node);
    }
    t.forEach(function (a) { r.push(a.toString()); });
    return r;
}
var TmkCache = /** @class */ (function () {
    function TmkCache(app) {
        this.app = app;
        // this.app.messenger.once("_tmkSetCacheSign", (r: any) => {
        //   if (typeof r == "string" && r.length) {
        //     this._tmkCacheSign = r;
        //   }
        // });
    }
    /** 获取全局缓存
     * @param node string|string[] 节点
     */
    TmkCache.prototype.getCache = function (node, method) {
        var _this = this;
        return new Promise(function (resolve) {
            // if (!n.length) return resolve({ err: "参数错误", raw: node });
            var s = {
                pid: process.pid,
                sign: method_1.uuid,
                path: getNode(node),
                method: method_1.touint(method)
            };
            _this.app.messenger.once(s.sign, resolve);
            _this.app.messenger.sendToAgent("_tmkGetCache", s);
        });
    };
    TmkCache.prototype.setCache = function (node, data, push) {
        var _this = this;
        return new Promise(function (resolve) {
            var s = {
                pid: process.pid,
                sign: method_1.uuid,
                path: getNode(node),
                value: data,
                push: push
            };
            _this.app.messenger.once(s.sign, resolve);
            _this.app.messenger.sendToAgent("_tmkSetCache", s);
        });
    };
    TmkCache.prototype.delCache = function (node, pop) {
        var _this = this;
        return new Promise(function (resolve) {
            var s = {
                pid: process.pid,
                sign: method_1.uuid,
                path: getNode(node),
                pop: pop
            };
            if (s.path.length) {
                _this.app.messenger.once(s.sign, resolve);
                _this.app.messenger.sendToAgent("_tmkDelCache", s);
            }
            else
                resolve(false);
        });
    };
    TmkCache.prototype.getHistory = function (node, method) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var path;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        path = getNode(node);
                        path.unshift("_tmkTimeData");
                        return [4 /*yield*/, this.getCache(path, method)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TmkCache.prototype.setHistory = function (node, data, push) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var path;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        path = getNode(node);
                        if (!path.length)
                            return [2 /*return*/, false];
                        else if (path.length == 1) {
                            if (!(data && Array.isArray(data.rooms))) {
                                return [2 /*return*/, false];
                            }
                        }
                        path.unshift("_tmkTimeData");
                        return [4 /*yield*/, this.setCache(path, data, push)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TmkCache.prototype.delHistory = function (node, pop) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var path;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        path = getNode(node);
                        if (!path.length)
                            return [2 /*return*/, false];
                        path.unshift("_tmkTimeData");
                        return [4 /*yield*/, this.delCache(path, pop)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return TmkCache;
}());
exports.TmkCache = TmkCache;
exports.default = (function (app) {
    app.cache = new TmkCache(app);
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFHQSw0Q0FBaUQ7QUFFakQsU0FBUyxPQUFPLENBQUMsSUFBMkI7SUFDMUMsSUFBTSxDQUFDLEdBQWEsRUFBRSxDQUFDO0lBQ3ZCLElBQUksQ0FBQyxHQUFTLEVBQUUsQ0FBQztJQUVqQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7UUFDdkIsQ0FBQyxHQUFHLElBQUksQ0FBQztLQUNWO1NBQU0sSUFBSSxPQUFPLElBQUksSUFBSSxRQUFRLEVBQUU7UUFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRTtnQkFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQUU7UUFDOUIsQ0FBQyxDQUFDLENBQUM7S0FDSjtTQUFNLElBQUksT0FBTyxJQUFJLElBQUksUUFBUSxFQUFFO1FBQ2xDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDZDtJQUNELENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDLElBQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzFDLE9BQU8sQ0FBQyxDQUFDO0FBQ1gsQ0FBQztBQUVEO0lBR0Usa0JBQWEsR0FBZ0I7UUFDM0IsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZiw0REFBNEQ7UUFDNUQsNENBQTRDO1FBQzVDLDhCQUE4QjtRQUM5QixNQUFNO1FBQ04sTUFBTTtJQUNSLENBQUM7SUFDRDs7T0FFRztJQUNILDJCQUFRLEdBQVIsVUFBUyxJQUEyQixFQUFFLE1BQWU7UUFBckQsaUJBWUM7UUFYQyxPQUFPLElBQUksT0FBTyxDQUFLLFVBQUEsT0FBTztZQUM1Qiw2REFBNkQ7WUFDN0QsSUFBTSxDQUFDLEdBQWE7Z0JBQ2xCLEdBQUcsRUFBRSxPQUFPLENBQUMsR0FBRztnQkFDaEIsSUFBSSxFQUFFLGFBQUk7Z0JBQ1YsSUFBSSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLE1BQU0sRUFBRSxlQUFNLENBQUMsTUFBTSxDQUFDO2FBQ3ZCLENBQUM7WUFDRixLQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztZQUN6QyxLQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BELENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELDJCQUFRLEdBQVIsVUFBUyxJQUFlLEVBQUUsSUFBUyxFQUFFLElBQVU7UUFBL0MsaUJBWUM7UUFYQyxPQUFPLElBQUksT0FBTyxDQUFVLFVBQUMsT0FBTztZQUNsQyxJQUFNLENBQUMsR0FBYTtnQkFDbEIsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHO2dCQUNoQixJQUFJLEVBQUUsYUFBSTtnQkFDVixJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQztnQkFDbkIsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsSUFBSSxFQUFFLElBQUk7YUFDWCxDQUFDO1lBQ0YsS0FBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDekMsS0FBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCwyQkFBUSxHQUFSLFVBQVMsSUFBZSxFQUFFLEdBQVM7UUFBbkMsaUJBYUM7UUFaQyxPQUFPLElBQUksT0FBTyxDQUFNLFVBQUMsT0FBTztZQUM5QixJQUFNLENBQUMsR0FBYTtnQkFDbEIsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHO2dCQUNoQixJQUFJLEVBQUUsYUFBSTtnQkFDVixJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQztnQkFDbkIsR0FBRyxFQUFFLEdBQUc7YUFDVCxDQUFDO1lBQ0YsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDakIsS0FBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3pDLEtBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDbkQ7O2dCQUFNLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDSyw2QkFBVSxHQUFoQixVQUFpQixJQUEyQixFQUFFLE1BQWU7Ozs7Ozt3QkFDckQsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDdEIscUJBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLEVBQUE7NEJBQXhDLHNCQUFPLFNBQWlDLEVBQUM7Ozs7S0FDMUM7SUFDSyw2QkFBVSxHQUFoQixVQUFpQixJQUFlLEVBQUUsSUFBUyxFQUFFLElBQVU7Ozs7Ozt3QkFDL0MsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNOzRCQUFFLHNCQUFPLEtBQUssRUFBQzs2QkFDMUIsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTs0QkFDekIsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQ3RDO2dDQUNBLHNCQUFPLEtBQUssRUFBQzs2QkFDZDt5QkFDRjt3QkFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUN0QixxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUE7NEJBQTVDLHNCQUFPLFNBQXFDLEVBQUM7Ozs7S0FDOUM7SUFDSyw2QkFBVSxHQUFoQixVQUFpQixJQUFlLEVBQUUsR0FBUzs7Ozs7O3dCQUNuQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU07NEJBQUUsc0JBQU8sS0FBSyxFQUFDO3dCQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUN0QixxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsRUFBQTs0QkFBckMsc0JBQU8sU0FBOEIsRUFBQzs7OztLQUN2QztJQUNILGVBQUM7QUFBRCxDQUFDLEFBN0VELElBNkVDO0FBN0VZLDRCQUFRO0FBK0VyQixtQkFBZSxVQUFDLEdBQWdCO0lBQzlCLEdBQUcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDaEMsQ0FBQyxFQUFDIn0=