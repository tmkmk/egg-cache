"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
// import { uuid } from "@mac-xiang/method";
// const sign = uuid;
var itf_1 = require("./itf");
var wl = new itf_1.LOG();
var cache = { _tmkTimeData: {} };
function cleanTime() {
    var a = cache._tmkTimeData, t = new Date().getTime();
    Object.keys(a).forEach(function (k) {
        var d = a[k];
        if (d.time && !(d.time > t)) {
            var dh_1 = d.history;
            if (dh_1) {
                Object.keys(dh_1).forEach(function (hk) {
                    wl.write(hk, dh_1[hk]);
                });
            }
            delete a[k];
        }
    });
}
setInterval(function () {
    try {
        cleanTime();
    }
    catch (error) {
        console.log("写日志发生严重错误", error);
    }
}, 60000);
function getCachePath(d) {
    if (Array.isArray(d) && d.length > 0) {
        var t = cache, i = void 0;
        while (d.length > 1) {
            i = d.shift();
            if (i) {
                if (!t[i])
                    t[i] = {};
                t = t[i];
            }
        }
        return t;
    }
}
//#region setTmkTimeData 无用折叠
/* function setTmkTimeData(d: setCache): boolean {
  const path = d.path, value = d.value;
  if (path[0] == "_tmkTimeData") {
    if (path[2] == "time" && cache._tmkTimeData[path[1]]) { // 上/离 线
      const rooms: string[] = [];
      rooms.push(path[1]);
      cache._tmkTimeData[path[1]].rooms.forEach(a => {
        rooms.push(a);
      });
      const time = new Date().getTime();
      let v = value as number;
      if (value && !(value > time)) v = time + 60000;
      cache._tmkTimeData[path[1]].time = v;

      const uid = rooms.shift() as string;
      const msg = {
        time: new Date().getTime(),
        type: "con",
        source: uid,
        content: value ? "离开会话" : "进入会话"
      };
      const uh = cache._tmkTimeData[uid].history;
      if (uh) { // 普通用户
        Object.keys(uh).forEach(con => { uh[con].push(msg); });
      } else { // 管理员
        Object.keys(cache._tmkTimeData).forEach(id => {
          const h = cache._tmkTimeData[id].history;
          if (h) {
            Object.keys(h).forEach(con => {
              if (h[con]) h[con].push(msg);
            });
          }
        });
      }
      return true;
    } else if (path.length == 4 && value && d.push) {
      if (path[2] == "history" && value.content == "创建会话") {
        if (!cache._tmkTimeData[path[1]]) {
          cache._tmkTimeData[path[1]] = {
            time: 0,
            rooms: Array.isArray(value.rooms) ? value.rooms : [path[3]],
          };
        }
        let th = cache._tmkTimeData[path[1]].history;
        if (!th) {
          th = {};
          cache._tmkTimeData[path[1]].history = th;
        }
        th[path[3]] = [value];
      }
    } else if (path.length == 3 &&
      cache._tmkTimeData[path[1]] &&
      path[0] == "_tmkTimeData" &&
      path[2] == "rooms" &&
      value &&
      (value.type == "join" || value.type == "leave") &&
      d.push) {
      const uh = cache._tmkTimeData[path[1]];
      const room = value.content as string;
      let rooms = uh.rooms;
      if (!Array.isArray(rooms)) {
        rooms = [];
        uh.rooms = rooms;
      }
      if (rooms.indexOf(room) < 0) {
        rooms.push(room);
      }
      if (uh.history) {
        let r = false;
        if (uh.history[room]) {
          uh.history[room].push(value);
          r = true;
        } else {
          const is = Object.keys(cache._tmkTimeData);
          for (let i = 0; i < is.length; i++) {
            const h = cache._tmkTimeData[is[i]].history;
            if (h) {
              const rs = Object.keys(h);
              for (let j = 0; j < rs.length; j++) {
                if (rs[j] == room) {
                  h[rs[j]].push(value);
                  r = true;
                }
              }
            }
            if (r) return r;
          }
        }
      }
    }
  }
  return false;
} */
//#endregion
exports.default = (function (agent) {
    // agent.messenger.on("egg-ready", () => { // 通知缓存进程,取信号
    //   agent.messenger.sendToApp("_tmkSetCacheSign", sign);
    // });
    agent.messenger.on("_tmkGetCache", function (d) { return tslib_1.__awaiter(void 0, void 0, void 0, function () {
        var ret, room, tr, ids, idi, history_1, rs, rsi;
        return tslib_1.__generator(this, function (_a) {
            ret = {};
            if ((d.method & 2) && d.path[0] == "_tmkTimeData") {
                room = d.path[1];
                tr = "";
                ids = Object.keys(cache._tmkTimeData);
                for (idi = 0; idi < ids.length; idi++) {
                    history_1 = cache._tmkTimeData[ids[idi]].history;
                    rs = Object.keys(history_1);
                    rsi = 0;
                    while (rsi < rs.length) {
                        if (rs[rsi] == room) {
                            tr = ids[idi];
                            break;
                        }
                        rsi++;
                    }
                    if (rsi < rs.length)
                        break;
                }
                ret[d.path[0]] = tr;
            }
            else {
                ret = getCachePath(d.path);
            }
            agent.messenger.sendTo(d.pid, d.sign, ret ? ret[d.path[0]] : undefined);
            if (ret && (d.method & 1) && d.path[0] != "_tmkTimeData") {
                delete ret[d.path[0]];
            }
            return [2 /*return*/];
        });
    }); });
    agent.messenger.on("_tmkSetCache", function (d) { return tslib_1.__awaiter(void 0, void 0, void 0, function () {
        var r, t;
        return tslib_1.__generator(this, function (_a) {
            r = false;
            t = getCachePath(d.path);
            if (t) {
                if (d.push) {
                    if (!t[d.path[0]])
                        t[d.path[0]] = [];
                    if (Array.isArray(t[d.path[0]])) {
                        t[d.path[0]].push(d.value);
                    }
                    else {
                        t[d.path[0]][Object.keys(t[d.path[0]]).length] = d.value;
                    }
                }
                else {
                    t[d.path[0]] = d.value;
                }
                r = true;
            }
            agent.messenger.sendTo(d.pid, d.sign, r);
            return [2 /*return*/];
        });
    }); });
    agent.messenger.on("_tmkDelCache", function (d) {
        var r;
        if (Array.isArray(d.path) && d.path.length > 0) {
            var t = getCachePath(d.path);
            if (t) {
                if (d.path[0] != "_tmkTimeData") {
                    if (d.pop) {
                        if (Array.isArray(t[d.path[0]]))
                            r = t[d.path[0]].pop();
                        else {
                            var k = Object.keys(t[d.path[0]]);
                            if (k.length) {
                                r = t[d.path[0]][k[k.length - 1]];
                                delete t[d.path[0]][k[k.length - 1]];
                            }
                        }
                    }
                    else {
                        r = t[d.path[0]];
                        delete t[d.path[0]];
                    }
                }
            }
        }
        agent.messenger.sendTo(d.pid, d.sign, r);
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWdlbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZ2VudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFFQSw0Q0FBNEM7QUFDNUMscUJBQXFCO0FBRXJCLDZCQUErRTtBQUUvRSxJQUFNLEVBQUUsR0FBRyxJQUFJLFNBQUcsRUFBRSxDQUFDO0FBQ3JCLElBQU0sS0FBSyxHQUFVLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxDQUFDO0FBQzFDLFNBQVMsU0FBUztJQUNoQixJQUFNLENBQUMsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3ZELE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUEsQ0FBQztRQUN0QixJQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDZixJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDM0IsSUFBTSxJQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUNyQixJQUFJLElBQUUsRUFBRTtnQkFDTixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEVBQUU7b0JBQ3hCLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLElBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixDQUFDLENBQUMsQ0FBQzthQUNKO1lBQ0QsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDYjtJQUNILENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQUNELFdBQVcsQ0FBQztJQUNWLElBQUk7UUFDRixTQUFTLEVBQUUsQ0FBQztLQUNiO0lBQUMsT0FBTyxLQUFLLEVBQUU7UUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztLQUNqQztBQUNILENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztBQUVWLFNBQVMsWUFBWSxDQUFDLENBQVc7SUFDL0IsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1FBQ3BDLElBQUksQ0FBQyxHQUFPLEtBQUssRUFBRSxDQUFDLFNBQW9CLENBQUM7UUFDekMsT0FBTyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNuQixDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2QsSUFBSSxDQUFDLEVBQUU7Z0JBQ0wsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDckIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNWO1NBQ0Y7UUFDRCxPQUFPLENBQUMsQ0FBQztLQUNWO0FBQ0gsQ0FBQztBQUNELDZCQUE2QjtBQUM3Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUE0Rkk7QUFDSixZQUFZO0FBQ1osbUJBQWUsVUFBQyxLQUFZO0lBRTFCLHdEQUF3RDtJQUN4RCx5REFBeUQ7SUFDekQsTUFBTTtJQUVOLEtBQUssQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxVQUFPLENBQVc7OztZQUMvQyxHQUFHLEdBQW1CLEVBQUUsQ0FBQztZQUM3QixJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLGNBQWMsRUFBRTtnQkFDM0MsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25CLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQ04sR0FBRyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUM1QyxLQUFTLEdBQUcsR0FBRyxDQUFDLEVBQUUsR0FBRyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7b0JBQ25DLFlBQVUsS0FBSyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFtQixDQUFDO29CQUMzRCxFQUFFLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFPLENBQUMsQ0FBQztvQkFDNUIsR0FBRyxHQUFHLENBQUMsQ0FBQztvQkFDWixPQUFPLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTSxFQUFFO3dCQUN0QixJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLEVBQUU7NEJBQ25CLEVBQUUsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7NEJBQ2QsTUFBTTt5QkFDUDt3QkFDRCxHQUFHLEVBQUUsQ0FBQztxQkFDUDtvQkFDRCxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTTt3QkFBRSxNQUFNO2lCQUM1QjtnQkFFRCxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUNyQjtpQkFBTTtnQkFDTCxHQUFHLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM1QjtZQUNELEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3hFLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLGNBQWMsRUFBRTtnQkFDeEQsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3ZCOzs7U0FDRixDQUFDLENBQUM7SUFDSCxLQUFLLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxjQUFjLEVBQUUsVUFBTyxDQUFXOzs7WUFHL0MsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUNSLENBQUMsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxFQUFFO2dCQUNMLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRTtvQkFDVixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7b0JBQ3JDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQy9CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDNUI7eUJBQU07d0JBQ0wsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO3FCQUMxRDtpQkFDRjtxQkFBTTtvQkFDTCxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7aUJBQ3hCO2dCQUNELENBQUMsR0FBRyxJQUFJLENBQUM7YUFDVjtZQUNELEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQzs7O1NBQzFDLENBQUMsQ0FBQztJQUNILEtBQUssQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxVQUFDLENBQVc7UUFDN0MsSUFBSSxDQUFNLENBQUM7UUFDWCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM5QyxJQUFNLENBQUMsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxFQUFFO2dCQUNMLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxjQUFjLEVBQUU7b0JBQy9CLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRTt3QkFDVCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQzs2QkFDbkQ7NEJBQ0gsSUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3BDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRTtnQ0FDWixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUNsQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs2QkFDdEM7eUJBQ0Y7cUJBQ0Y7eUJBQU07d0JBQ0wsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2pCLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDckI7aUJBQ0Y7YUFDRjtTQUNGO1FBQ0QsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzNDLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxFQUFDIn0=