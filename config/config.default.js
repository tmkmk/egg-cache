'use strict';

/**
 * egg-cache default config
 * @member Config#cache
 * @property {String} SOME_KEY - some description
 */
exports.cache = {
  app: true,
  agent: true,
};
