export interface oa {
    [propName: string]: any;
}
export declare function mkdir(path: string): void;
export declare class LOG {
    private dir;
    private suffix;
    constructor(filePath?: string, suffix?: string);
    getPath(filePath?: string): string;
    getFileName(p: string): string;
    readLog(file: string): oa[] | undefined;
    write(file: string, data: any, line?: number, insert?: boolean): void;
    get newLog(): string;
}
interface cacheSign {
    pid: number;
    sign: string;
    path: string[];
}
export interface setCache extends cacheSign {
    value: any;
    push?: any;
}
/** 读取方法method值 【比特位】 如下:
 * @1: 读取后删除; 设置比特位2时无效
 * @2: 读取_tmkTimeData数据时有效,根据传递path的第二个元素查找history子项,返回拥有此参数的父级key
 */
export interface getCache extends cacheSign {
    method: number;
}
export interface delCache extends cacheSign {
    pop?: any;
}
export declare type ns = number | string;
export interface funGetCache {
    (param: undefined | ns | ns[]): Promise<oa>;
}
export interface funSetCache {
    (node: ns | ns[], data: any): Promise<boolean>;
}
export interface funDelCache {
    (node: ns | ns[]): Promise<boolean>;
}
export interface HistoryS {
    [node: string]: Array<any>;
}
export interface TmkTimeData {
    time?: number;
    rooms: string[];
    history?: HistoryS;
    [node: string]: any;
}
export interface Cache {
    _tmkTimeData: {
        [node: string]: TmkTimeData;
    };
    [node: string]: any;
}
export {};
