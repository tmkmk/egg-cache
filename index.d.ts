import 'egg';
import { TmkCache } from "./app";

declare module 'egg' {
  export interface Application {
    cache: TmkCache;
  }
}
