# egg-cache

#### 介绍

egg 的缓存.如果有任何问题,请用最善良的心以及最恶毒的语言进行批评.

#### 软件架构

编译: tsc
测试: 没有

#### 安装教程

1.  npm i @mac-xiang/egg-cache"
2.  plugin.ts

```typescript
  cache: {
    enable: true,
    package:"@mac-xiang/egg-cache"
  }
```

3.  typings/index.d.ts

```typescript
import { funGetCache, funSetCache } from "@mac-xiang/egg-cache/itf.ts";

declare module "egg" {
  export interface Application {
    config: EggAppConfig & DefaulConfig;
    bar: string;
    getCache: funGetCache;
    setCache: funSetCache;
  }
}
```

#### 使用说明

1.  app.getCache(node,once?:any)
2.  app.setCache(node,data)
3.  app.delCache(node)

    node:节点.数据类型可以是字符串,也可以是字符串数组.
    例如:缓存为 {a:{b:{c:{d:123}}}}; 表示缓存下 a.b.c.d=123;node 则可以是"a.b.c.d",data 则为 123.
    如果节点为"a.b"则 data 为{c:{d:123};节点还可以写成 ["a","b","c","d"]的形式.
    getCache 为 promise.请使用 await 修饰符.
    once: 是否读取后删除.非0/空 为真

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

啥都没有.就自己用.自己能看懂就行.就几个方法,自己去 index.js 中看吧.或者 src index.ts 中看,有注解.

npm 上传包命令
npm publish --registry http://registry.npmjs.org
npm config set registry https://registry.npmjs.org
npm publish --access public  --registry http://registry.npmjs.org
npm config set scope mac-xiang --global

npm version prerelease

npm version prepatch
别笑,记不住

#### 更新日志
1.0.1-13:
修复 根目录没有logs时报错.
1.0.1-10:
修复上一版逻辑错误.
1.0.1-9:
修复bug.读取_tmkTimeData数传递参数method=2时,没有查找到正确数据的逻辑错误.
1.0.1-6:
package.json配置错误,导致安装依赖包以后提示itf.ts之类的错误.将files:成员itf.ts改为itf.js
1.0.1-5:
重新规范名称.默认附加的this.app.tmkCache改为this.app.cache
1.0.1-3:
新增删除缓存 delCache 方法;
在 getCache 中加入参数 once.非 0/空为 true,读取后删除此数据.

1.0.1-2:
修复 bug:写缓存时,数据类型为非 object 时会出现数据错乱.

基本没啥用.补充一下.1.0.0 与 1.0.1-1 之间经历了什么? 第一次:忘了在 package.json 中添加 itf.ts 了.导致安装 egg-cache 包后, 没法引入.凎~! 第二次呢? 是我的 readme.md 写错了. 是@mac-xiang/egg-cache 而不是 @mac-xiang/cache
完事.打完收工
