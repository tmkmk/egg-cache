import { Application } from 'egg';
import { oa, ns } from './itf';
export declare class TmkCache {
    app: Application;
    constructor(app: Application);
    /** 获取全局缓存
     * @param node string|string[] 节点
     */
    getCache(node: undefined | ns | ns[], method?: number): Promise<oa>;
    setCache(node: ns | ns[], data: any, push?: any): Promise<boolean>;
    delCache(node: ns | ns[], pop?: any): Promise<any>;
    getHistory(node: undefined | ns | ns[], method?: number): Promise<oa>;
    setHistory(node: ns | ns[], data: any, push?: any): Promise<boolean>;
    delHistory(node: ns | ns[], pop?: any): Promise<any>;
}
declare const _default: (app: Application) => void;
export default _default;
